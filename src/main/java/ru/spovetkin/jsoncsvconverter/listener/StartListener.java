package ru.spovetkin.jsoncsvconverter.listener;

import lombok.RequiredArgsConstructor;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.spovetkin.jsoncsvconverter.service.ConverterService;

import java.io.IOException;


@Component
@RequiredArgsConstructor
public class StartListener {

    private final ConverterService converterService;

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) throws IOException {
        converterService.convertJsonToCsv();
    }

}
