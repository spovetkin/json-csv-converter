package ru.spovetkin.jsoncsvconverter.service;

import java.io.IOException;

/**
 * Сервис конвертации
 */
public interface ConverterService {

    void convertJsonToCsv() throws IOException;

}
